import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./scss/main.scss";
import HomePage from "./pages/Home";
import ProductDetail from "./components/ProductDetails";
import AddProduct from "./components/AddProduct";
import "antd-mobile/dist/antd-mobile.css";
import "antd/dist/antd.css";
import "./global.scss";

export let toggleLoginModal;

function App() {
  const [show, setShow] = React.useState(false);
  React.useEffect(() => {
    toggleLoginModal = () => setShow(show => !show);
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/item/:id" exact component={ProductDetail} />
        <Route path="/AddProduct" exact component={AddProduct} />
        <Route path="/" exact component={HomePage} />
      </Switch>
    </Router>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
