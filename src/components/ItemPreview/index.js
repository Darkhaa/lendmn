import React from "react";
import ProductCard from "../ProductCard";
import { Link } from "react-router-dom";
import { NavBar } from "antd-mobile";
import { auctionList } from "../../restAPI/main";

function ItemPreview() {
  const [page, setPage] = React.useState(1);
  const [data, setData] = React.useState([]);
  
  React.useEffect(() => {
    auctionList(page.toString(), data => setData(data), () => {}, "GET");
  }, [page]);

  return (
    <div>
      <NavBar
        style={{
          marginBottom: "10px",
          backgroundColor: "white",
          borderBottom: "groove",
          height: "60px"
        }}
        mode="white"
        rightContent={<Link to='/AddProduct'>Auction нэмэх</Link>}
        leftContent={<img width="70" src="https://scontent.fuln1-1.fna.fbcdn.net/v/t1.15752-9/60755756_305043750414967_9143115229989699584_n.png?_nc_cat=101&_nc_ht=scontent.fuln1-1.fna&oh=8ce814f89dd5ade0301613337e0d3d2c&oe=5D668BDC" />}
      >
      </NavBar>
      <div className="card">
        <div className="card-body">
          <div className="utils__title mb-4 text-uppercase">
            <h4><strong>Products Catalog</strong></h4>
          </div>
          <div className="productsCatalog">
            <div className="row">
              {data.map((data, i)=>{
                return <div key={i} className="col-xl-4 col-lg-6 col-md-12">
                  <ProductCard {...data} />
                </div>;
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemPreview;
