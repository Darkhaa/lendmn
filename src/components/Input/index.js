import React from "react";

import { injectClasses } from "../../util";
import styles from "./styles.m.scss";

function Input(props) {
  return <input {...injectClasses(styles.input, props)} />;
}

export default Input;
