import React from "react";
import moment from "moment";
import S3FileUpload from 'react-s3';
import { Link } from "react-router-dom";
import { auctionAdd } from "../../restAPI/main"
import { Form, Input, Button, Upload, Icon, DatePicker, InputNumber, message as notification } from "antd";

const { RangePicker } = DatePicker

const config = {
  bucketName: 'fibo-ticket-attachments',
  region: 'us-east-1',
  accessKeyId: 'AKIAIBD2WLFWBTOOZRMQ',
  secretAccessKey: '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',
}


class ItemPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previewVisible: false,
      previewImage: "",
      fileList: [],
      photoArray : [],
      startDate : "",
      endDate : "",

    };
    this.dummyRequest = this.dummyRequest.bind(this)
  }

  dummyRequest = ({ file, onSuccess, onError }) => {
    S3FileUpload.uploadFile(file, config)
      .then(data => {
        console.log(data);
        this.setState(prevState => ({
          photoArray: [...prevState.photoArray, data.location]
        }))

        onSuccess('ok')
      })
      .catch(err => {
        console.log(err)
        onError(err)
      })
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { startDate, endDate, photoArray } = this.state;
        const item = {
          Photos : photoArray,
          Title : values.title,
          Description : values.description
        }

        const request = {
          item,
          startTime : +startDate,
          endTime : +endDate,
          initialPrice : values.InitialPrice,
          minimalBid  : values.MinimalBid,
          withdrawalInterval : values.WithdrawalInterval
        }
        console.log(request);
        console.log("Received values of form: ", values);
        auctionAdd(request, this.onSuccess, this.onFailed, "POST");
        
      }
    });
  };

  onSuccess = response => {
    notification.success("Амжилттай нэмлээ")
  }

  onFailed = err => {
    notification.error("Алдаа гарлаа")
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  handleChange = ({ fileList }) => this.setState({ fileList });

  onChangeDate = e => {
    console.log(e);
    console.log(moment(e[0]._d).format("X"))
    this.setState({
      startDate : moment(e[0]._d).format("X"),
      endDate : moment(e[1]._d).format("X")
    })
  }

  render() {
    const { fileList, photoArray } = this.state;
    console.log(photoArray);
    const { getFieldDecorator } = this.props.form;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <div className="card">
        <div className="card-body">
          <div className="utils__title mb-4 text-uppercase">
            <h4>
              <strong>Add Product</strong>
            </h4>
          </div>
          <div className="productsCatalog">
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item label="Title">
                {getFieldDecorator("title", {
                  rules: [
                    { required: true, message: "Please input your title!" }
                  ]
                })(<Input />)}
              </Form.Item>
              <Form.Item label="Phonos">
                {getFieldDecorator("phonos", {
                  rules: [
                    { required: false, message: "Please choose your phonos" }
                  ]
                })(
                  <Upload customRequest={this.dummyRequest}
                    listType="picture-card"
                  >
                    {fileList.length >= 3 ? null : uploadButton}
                  </Upload>
                )}
              </Form.Item>
              <Form.Item label="Description">
                {getFieldDecorator("description", {
                  rules: [
                    { required: true, message: "Please input your description!" }
                  ]
                })(<Input />)}
              </Form.Item>
              <Form.Item label="Initial Price">
                {getFieldDecorator("InitialPrice", {
                  rules: [
                    { required: true, message: "Please input your Price!" }
                  ]
                })(<InputNumber style={{width : "100%"}}/>)}
              </Form.Item>

              <Form.Item label="Minimal Bid">
                {getFieldDecorator("MinimalBid", {
                  rules: [
                    { required: true, message: "Please input your mininal bid!", type: "number"  }
                  ]
                })(<InputNumber style={{width : "100%"}}/>)}
              </Form.Item>

              <Form.Item label="Dates">
                {getFieldDecorator("EndTime", {
                  rules: [
                    {
                      type: "array",
                      required: true,
                      message: "Please select time!"
                    }
                  ]
                })(<RangePicker onChange={this.onChangeDate}/>)}
              </Form.Item>

              <Form.Item label="Withdrawal Interval">
                {getFieldDecorator("WithdrawalInterval", {
                  rules: [
                    { required: true, message: "Please input your withdrawal interval!" }
                  ]
                })(<InputNumber style={{width : "100%"}}/>)}
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Save
                </Button>{"             "}
                <Link to='/'>
                  Back
                </Link>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

export default Form.create()(ItemPreview);
