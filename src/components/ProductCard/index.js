import React from "react";
import { Link } from "react-router-dom";

import styles from "./style.module.scss";

function ProductCard(props) {
  console.log(props);
  return (
    <Link to={`/item/${props.id}`}>
      <div className={styles.productCard}>
        <div className={styles.img}>
          <div className={styles.like}>
            <i className="icmn-heart" />
          </div>
          <img src={props.item.photos[0]} alt="" />
        </div>
        <div className={styles.title}>
          {props.item.name}
          <div className={styles.price}>
            {props.currentBid}
            <div className={styles.oldPrice}>{props.initialPrice}</div>
          </div>
        </div>
        <div className={styles.descr}>
          {props.item.description}
        </div>
      </div>
    </Link>
  );
}

export default ProductCard;
