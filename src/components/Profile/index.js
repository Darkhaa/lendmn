import React from "react";
import { NavBar } from "antd-mobile";
import ProductList from "../ProductList";
import { Badge, Divider, Typography } from "antd";
import styles from "./styles.m.scss";

const { Text } = Typography;

function Profile() {
  return (
    <div>
      <div className={styles.content}>
        <div className={styles.details}>
          <Divider>Хэрэглэгчийн мэдээлэл</Divider>
          <h5>Нэр : <strong>Darkhanbayar</strong></h5>
          <h5>Утас : <strong>80051530</strong></h5>
          <Divider>Дуудлага худалдаа</Divider>
          <ProductList />
        </div>
      </div>
    </div>
  );
}

export default Profile;
