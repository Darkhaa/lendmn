import React, { useState } from "react";
import { TabBar } from "antd-mobile";
import ItemPreview from "../ItemPreview";
import AddProduct from "../AddProduct";
import Profile from "../Profile";

function getParam(name) {
  if (name = (new RegExp("[?&]" + encodeURIComponent(name) 
    + "=([^&]*)")).exec(location.search))
    return decodeURIComponent(name[1]);
}
function checkToken(cb) {
  const token = localStorage.getItem("token");
  if (!token) {
    let code = getParam("code");
    fetch(`http://35.221.126.175/api/users/login?code=${code}`)
      .then(res => res.json())
      .then(cb);
  }
}

export function Menu() {
  const [page, setPage] = useState(0);
  const [hidden, setHidden] = useState();
  const [fullScreen, setFullScreen] = useState(true);
  const [selectedTab, setSelectedTab] = useState("home");

  React.useEffect(() =>
    checkToken(({ token }) => 
      setTokenString(localStorage.setItem("token", token)), []));

  return (
    <div
      style={
        fullScreen
          ? { position: "fixed", height: "100%", width: "100%", top: 0 }
          : { height: 400 }
      }
    >
      <TabBar
        unselectedTintColor="#949494"
        tintColor="#33A3F4"
        barTintColor="white"
        hidden={hidden}
      >
        <TabBar.Item
          title="Нүүр"
          key="Нүүр"
          icon={<i className="fa fa-home"></i>}
          selectedIcon={<i className="fa fa-home"></i> }
          selected={selectedTab === "home"}
          badge={1}
          onPress={() => {
            setSelectedTab("home");
          }}
          data-seed="logId"
        >
          <ItemPreview />
        </TabBar.Item>
        <TabBar.Item
          icon={<i className="fa fa-user"></i>}
          selectedIcon={<i className="fa fa-user"></i>}
          title="Профайл"
          key="Profile"
          dot
          selected={selectedTab === "profile"}
          onPress={() => {
            setSelectedTab("profile");
          }}
        >
          <Profile />
        </TabBar.Item>
      </TabBar>
    </div>
  );
}
