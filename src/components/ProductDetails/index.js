import React, { Component } from "react";
import {
  Carousel,
  Breadcrumb,
  Rate,
  Select,
  Tooltip,
  Button,
  Icon,
  Tabs
} from "antd";
import { NavBar } from "antd-mobile";
import styles from "./style.module.scss";
import data from "./data.json";

const { TabPane } = Tabs;
const { Option } = Select;

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgActiveStatus: [],
      images: props.item.photos,
      name: props.item.name,
      price: props.currentBid,
      oldPrice: props.initialPrice,
      description: props.item.description,
    };
  }

  setActiveImage = index => {
    this.state.images.map()
  };

  componentWillMount() {
    const { imgActiveStatus, images } = this.state;
    images.forEach((img, index) => {
      imgActiveStatus[index] = "not-active";
      if (index === 0) imgActiveStatus[0] = "active";
    });
  }

  setActiveImg = imgNumber => {
    const { imgActiveStatus } = this.state;
    imgActiveStatus.forEach((imgStatus, index) => {
      imgActiveStatus[index] = "not-active";
      if (imgNumber === index) {
        imgActiveStatus[index] = "active";
      }
    });
    this.setState({
      imgActiveStatus
    });
  };

  refSlider = node => {
    this.slider = node;
  };

  changeSlide = (e, index) => {
    e.preventDefault();
    this.slider.slick.innerSlider.slickGoTo(index);
    this.setActiveImg(index);
  };

  render() {
    const {
      imgActiveStatus,
      images,
      sku,
      name,
      rate,
      price,
      oldPrice,
      shortDescr,
      description,
      properties
    } = this.state;

    return (
      <div>
        <NavBar
          style={{
            marginBottom: "10px",
            backgroundColor: "white",
            borderBottom: "groove",
            height: "60px"
          }}
          mode="white"
          rightContent={
            <span>
              sdasdasdsa
            <i className="fa fa-user"></i>
            </span>
          }
          leftContent={<img width="70" src="https://scontent.fuln1-1.fna.fbcdn.net/v/t1.15752-9/60755756_305043750414967_9143115229989699584_n.png?_nc_cat=101&_nc_ht=scontent.fuln1-1.fna&oh=8ce814f89dd5ade0301613337e0d3d2c&oe=5D668BDC" />}
        >
        </NavBar>
        <section className="card">
          <div className="card-body">
            <div className="utils__title mb-4 text-uppercase">
              <h4><strong>Products Detail</strong></h4>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className={styles.item}>
                  <div className={styles.img}>
                    <div className={styles.status}>
                      <span className={styles.statusTitle}>New</span>
                    </div>
                    <div className={`${styles.like} ${styles.selectedLike}`}>
                      <i className="fa fa-heart" />
                    </div>
                    <Carousel
                      ref={this.refSlider}
                      autoplay={false}
                      dots={false}
                      effect="fade"
                    >
                      {images.map(image => (
                        <div key={image}>
                          <img className={styles.img} src={image} alt="" />
                        </div>
                      ))}
                    </Carousel>
                  </div>
                </div>
                <div className={`${styles.photos} clearfix`}>
                  {images.map((image, index) => (
                    <a
                      href="javascript: void(0)"
                      key={image}
                      onClick={e => {
                        this.changeSlide(e, index);
                      }}
                      className={`${styles.photosItem} ${
                        imgActiveStatus[index] === "active"
                          ? styles.photosItemActive
                          : ""
                        }`}
                    >
                      <img src={image} alt="" />
                    </a>
                  ))}
                </div>
              </div>
              <div className="col-lg-8">
                <div className={styles.sku}>
                  {`USER: #${sku}`}
                  <br />
                  <div className={styles.raiting}>
                    <Rate value={rate} disabled allowHalf />
                  </div>
                </div>
                <h4 className={styles.mainTitle}>
                  <strong>{name}</strong>
                </h4>
                <div className={styles.price}>
                  {`$${price}`}
                  {oldPrice && (
                    <div className={styles.priceBefore}>{`First price $${oldPrice} `}</div>
                  )}
                </div>
                <hr />
                <div className={`mb-1 ${styles.descr}`}>
                  <p>{shortDescr}</p>
                </div>
                <div className={styles.controls}>
                  <Button type="primary" size="large">
                    <Icon type="shopping-cart" />
                    Bit Now
                  </Button>
                </div>
                <div className={styles.info}>
                  <Tabs defaultActiveKey="1">
                    <TabPane tab="Information" key="1">
                      {properties.map(property => (
                        <div className="mb-1" key={property.name}>
                          <strong className="mr-1">{`${
                            property.name
                            }: `}</strong>
                          {property.value}
                        </div>
                      ))}
                    </TabPane>
                    <TabPane tab="Description" key="2">
                      <p>{description}</p>
                    </TabPane>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default ProductDetails;
