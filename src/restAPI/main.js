import * as config from "./config.json";

function baseBodyFetch(url, request, successCallback, failCallback, rMethod) {
  fetch(config.BASE_URL + url, {
    method: rMethod,
    headers: {
      "X-Auth-Token": localStorage.getItem("token")
    },
    body: rMethod !== "GET" ? JSON.stringify(request) : null
  })
    // eslint-disable-next-line consistent-return
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      if (response.status === 401) {
        localStorage.clear();
        window.location.href = "/";
      }
      failCallback(response.statusText);
    })
    .then(data => {
      if (data !== undefined) {
        if (data.error === null) {
          SuccessResponse(data, successCallback);
        } else {
          errorJob(data, failCallback);
        }
      }
    })
    .catch(error => console.error("Error:", error));
}

function SuccessResponse(data, successCallback) {
  if (data.data !== null) {
    successCallback(data.data);
  } else {
    successCallback([]);
  }
}

function errorJob(error, failCallback) {
  // const jsonError = JSON.parse(error.Error.split("error message:")[1]);
  if (error.Error === "Authentication failed") {
    localStorage.clear();
    window.location = "/";
  }
  failCallback(error.error);
}



export function auctionAdd(request, successCallback, failCallback, rMethod) {
  baseBodyFetch("auction/add", request, successCallback, failCallback, rMethod);
  return "";
}

export function auctionList(page, successCallback, failCallback, rMethod) {
  baseBodyFetch("auction/list?page="+page, "", successCallback, failCallback, rMethod);
  return "";
}

