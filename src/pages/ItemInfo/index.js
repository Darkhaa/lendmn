import React from "react";

import styles from "./styles.m.scss";

function ItemInfoPage({ match: { params: { id } } }) {
  if (!id) return null;

  const [info, setInfo] = React.useState(null);
  React.useEffect(() => {
    setInfo({
      id: "1",
      image: "https://cdn.unegui.mn/cache/a3/b2/a3b2abf19fbef1db6a67c1ee2016187c.jpg",
      title: "Redmi note 7",
      price: 500000,
      description: "sayhan avsan har ongiin ymarch asuudalgui redmi note 7 zarna"
    });
  }, [id]);

  if (!info) return null;
  return (
    <div className={styles.content}>
      <img className={styles.image} src={info.image} />
      <div className={styles.details}>
        <span className={styles.title}>{info.title}</span>
        <span className={styles.price}>{info.price}₮</span>
        <span className={styles.description}>{info.description}</span>
      </div>
    </div>
  );
}

export default ItemInfoPage;
