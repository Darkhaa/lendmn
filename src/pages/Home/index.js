import React from "react";
import styles from "./styles.m.scss";
import { Menu } from "../../components/BottomMenu";

function HomePage() {
  return (
    <div className={styles.content}>
      <Menu />
    </div>
  );
}

export default HomePage;
