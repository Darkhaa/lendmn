export function injectClasses(className, props) {
  const classes =
    props.className ? className + " " + props.className : className;
  return { ...props, className: classes };
}
