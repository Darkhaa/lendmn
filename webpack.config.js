const nicepack = require("@nicepack/core");
const config = nicepack(require("@nicepack/react"));

config.devServer.host = "0.0.0.0";
config.devServer.overlay = true;
config.devServer.compress = true;
config.devServer.disableHostCheck = true;
config.devServer.historyApiFallback = true;

module.exports = config;
